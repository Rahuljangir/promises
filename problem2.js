let fs = require('fs');
let path = require('path');

function problem2(lipsumPath) {

    function readLipsumFile(lipsumPath) {
        let readFilePromise = new Promise((resolve, reject) => {
            fs.readFile(lipsumPath, 'utf8', (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    console.log("Read the lipsumFile.txt file.");
                    resolve(data);
                }
            })
        });
        return readFilePromise;
    }


    function writeInUpperCase(data) {

        let dataInUppercase = data.toUpperCase();
        let fileName = 'upperCase.txt';
        let filePath = `./data/${fileName}`;
        let pathOfFile = path.join(__dirname, filePath);

        let upperCasePromise = new Promise((resolve, reject) => {
            fs.writeFile(pathOfFile, dataInUppercase, (err) => {
                if (err) {
                    reject(err);
                } else {
                    console.log(`File ${fileName} write successfully`);
                    writeFileName(fileName);
                    resolve(dataInUppercase);
                }
            });
        })
        return upperCasePromise;
    }


    function writeInLowerCase(data) {
        let dataInLowercase = data.toLowerCase()
            .split('.');
        
        let sentence = dataInLowercase.join('.\n');
        let fileName = 'lowerCase.txt';
        let filePath = `./data/${fileName}`;
        let pathOfFile = path.join(__dirname, filePath);

        let lowerCasePromise = new Promise((resolve, reject) => {
            fs.writeFile(pathOfFile, sentence, (err) => {
                if (err) {
                    reject(err);
                } else {
                    console.log(`File ${fileName} write successfully`);
                    writeFileName(fileName);
                    resolve(pathOfFile);
                }
            });
        });

        return lowerCasePromise;
    }


    function sortTheContent(pathOfFile) {
        let sortingPromise = new Promise((resolve, reject) => {
            fs.readFile(pathOfFile, 'utf8', (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    console.log("read the new file");
                    let sortedData = data.split(' ')
                        .sort().join(' ');
                    let fileName = "sortedContent.txt";
                    let filePath = `./data/${fileName}`;
                    let pathOfFile = path.join(__dirname, filePath);

                    fs.writeFile(pathOfFile, sortedData, (err) => {
                        if (err) {
                            reject(err);
                        } else {
                            console.log(`file ${fileName} write successfully.`);
                            resolve();
                        }
                    })
                    resolve(fileName)
                }

            })
        })
        return sortingPromise;
    }


    let count = 0
    function writeFileName(fName) {
        let fileName = "fileName.txt";
        let filePath = `./data/${fileName}`;
        let pathOfFile = path.join(__dirname, filePath);

        let writeFile = new Promise((resolve, reject) => {
            fs.appendFile(pathOfFile, ' ' + fName, (err) => {
                if (err) {
                    reject(err);
                } else {
                    console.log(`file ${fName} written in ${fileName} file`)
                    count++;
                    if (count === 3) {
                        resolve(pathOfFile);
                    }
                }
            })
        })

        return writeFile;
    }


    function readAndDelete(fPath) {
        let readPromise = new Promise((resolve, reject) => {
            fs.readFile(fPath, 'utf8', (err, data) => {
                if (err) {
                    reject(err);
                } else {

                    console.log("fileName.txt read successfully");
                    let dirPath = fPath.replace('fileName.txt', '');
                    let fileName = data.trim()
                        .split(' ');

                    let deletedFile = fileName.map((curr) => {
                        fs.unlink(`${dirPath}${curr}`, (err) => {
                            if (err) {
                                reject(err);
                            } else {
                                console.log(`${curr} file deleted successfully`);
                                resolve();
                            }
                            return curr;
                        })
                    })
                }
            })
        })
        return readPromise;
    }


    readLipsumFile(lipsumPath).then((data) => {
        return writeInUpperCase(data);
    })
    .then((upperCaseData) => {
        return writeInLowerCase(upperCaseData);
    })
    .then((lowerCaseData) => {
        return sortTheContent(lowerCaseData);
    })
    .then((fileName) => {
        return writeFileName(fileName);
    })
    .then((pathOfFile) => {
        return readAndDelete(pathOfFile);
    })
    .catch((err) => {
        console.log(err);
    })
}

module.exports = problem2;