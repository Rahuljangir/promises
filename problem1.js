let fs = require("fs");
let path = require("path");

function problem1(directory) {
     function createDir(directory) {

          let promise = new Promise((resolve, reject) => {
               fs.mkdir(directory, (err) => {
                    if (err) {
                         reject(err);
                    } else {
                         resolve("Directory create successfully");
                    }
               });
          });
          
          return promise;
     }

     let random = Math.floor(Math.random() * 10);

     function createRandomFile() {
          let index = 0;

          let intervalId = setInterval(() => {
               let fileName = `Hello${index}.JSON`;
               let filePath = `./test/${directory}/${fileName}`;
               let pathOfFile = path.join(__dirname, filePath);

               let createPromise = new Promise((resolve, reject) => {
                    fs.writeFile(pathOfFile, "Hello World", (err) => {
                         if (err) {
                              reject(err);
                         } else {
                              console.log(`File ${fileName} created successfully`);
                              resolve();
                         }
                    });
               })

               if (index === random) {
                    clearInterval(intervalId);
               }
               index++;
               
               return createPromise;
          }, 500);
     }

     function deletedFile() {
          let count = 0;
          setTimeout(function () {

               let intervalId = setInterval(() => {
                    let fileName = `Hello${count}.JSON`;
                    let filePath = `./test/${directory}/${fileName}`;
                    let pathOfFile = path.join(__dirname, filePath);

                    let deletedFilePromise = new Promise((resolve, reject) => {
                         fs.unlink(pathOfFile, (err) => {
                              if (err) {
                                   reject(err);
                              } else {
                                   console.log(`File ${fileName} deleted successfully`);
                                   resolve();
                              }
                         });
                    })

                    if (count === random) {
                         clearInterval(intervalId);
                    }
                    count++;

                    return deletedFilePromise;
               }, 500)
          }, 5000)
     }


     createDir(directory).then((data) => {
          console.log(data);
          return createRandomFile();
     })
     .then(() => {
          return deletedFile();
     })
     .catch((err) => {
          console.log(err);
     })
}

module.exports = problem1;